package com.example.guardianapicodingchallenge.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.guardianapicodingchallenge.R
import com.example.guardianapicodingchallenge.databinding.GuardianItemLayoutBinding
import com.example.guardianapicodingchallenge.model.NewsFeedResponse
import com.example.guardianapicodingchallenge.model.ResultsInfo

import com.squareup.picasso.Picasso

class NewsAdapter(
    val dataSet: NewsFeedResponse,
    val listener: (url: String) -> Unit
) : RecyclerView.Adapter<NewsAdapter.NewsViewHolder>() {
    class NewsViewHolder(val newsView: View) : RecyclerView.ViewHolder(newsView) {
        private val thumbNailNews: ImageView = newsView.findViewById(R.id.iv_item_thumbnail)
        private val headerLineNews: TextView = newsView.findViewById(R.id.tv_item_headline)
        private val trialTextNews: TextView = newsView.findViewById(R.id.tv_item_trail_text)

        fun onBind(openUrl: (url: String) -> Unit, result: ResultsInfo) {
            headerLineNews.text = result.fields.headline
            trialTextNews.text = result.fields.trailText
            Picasso.get().load(result.fields.thumbnail).into(
                thumbNailNews
            )
            newsView.setOnClickListener {
                openUrl.invoke(result.webUrl)
            }
        }
    }

//    private var _binding: GuardianItemLayoutBinding? = null
//    private val binding
//        get() = _binding!!

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsViewHolder {
//        _binding = GuardianItemLayoutBinding.inflate(
//            LayoutInflater.from(parent.context),
//            parent,
//            false
//        )
//        return NewsViewHolder(binding.root)
        return NewsViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.guardian_item_layout, parent, false))
    }

    override fun getItemCount() = dataSet.response.results.size

    override fun onBindViewHolder(holder: NewsViewHolder, position: Int) {
        holder.onBind(listener, dataSet.response.results[position])
    }
}