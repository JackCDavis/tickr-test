package com.example.guardianapicodingchallenge.di.component

import com.example.guardianapicodingchallenge.MainActivity
import com.example.guardianapicodingchallenge.di.modules.NetworkModule
import dagger.Component

@Component(modules = [NetworkModule::class])
interface GuardianComponent {
    fun inject(activity: MainActivity)
}