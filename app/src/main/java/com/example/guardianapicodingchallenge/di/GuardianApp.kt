package com.example.guardianapicodingchallenge.di

import android.app.Application
import com.example.guardianapicodingchallenge.di.component.DaggerGuardianComponent
import com.example.guardianapicodingchallenge.di.component.GuardianComponent
import com.example.guardianapicodingchallenge.di.modules.NetworkModule

class GuardianApp: Application() {

    override fun onCreate() {
        super.onCreate()
        guardianComponent = DaggerGuardianComponent.builder()
            .networkModule(NetworkModule())
            .build()
    }
    companion object{
        lateinit var guardianComponent: GuardianComponent
    }
}