package com.example.guardianapicodingchallenge

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.View
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.guardianapicodingchallenge.databinding.ActivityMainBinding
import com.example.guardianapicodingchallenge.di.GuardianApp
import com.example.guardianapicodingchallenge.model.GuardianAppState
import com.example.guardianapicodingchallenge.model.GuardianRepository
import com.example.guardianapicodingchallenge.model.NewsFeedResponse
import com.example.guardianapicodingchallenge.view.NewsAdapter
import com.example.guardianapicodingchallenge.viewmodel.GuardianViewModel
import com.example.guardianapicodingchallenge.viewmodel.GuardianViewModelProvider
import javax.inject.Inject

private const val TAG = "MainActivity"

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModelProvider: GuardianViewModelProvider

    val viewModel: GuardianViewModel by lazy {
        viewModelProvider.create(GuardianViewModel::class.java)
    }

    lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        GuardianApp.guardianComponent.inject(this)
        initBinding()
        setContentView(binding.root)
        initView()
        observation()
    }

    private fun observation() {
        viewModel.getNewsFeedData().observe(this, Observer {
            Log.d(TAG, "observation: ")
            when (it) {
                is GuardianAppState.RESPONSE -> {
                    updateRv(it.dataSet)
                }
                is GuardianAppState.LOADING -> {
                    hideLoadingBar()
                }
                is GuardianAppState.ERROR -> {
                    Toast.makeText(
                        this@MainActivity,
                        it.errorMessage,
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        })
    }

    private fun hideLoadingBar() {
        Log.d(TAG, "hideLoadingBar: ${binding.progressBar.visibility}")
        binding.progressBar.visibility =
            if (binding.progressBar.visibility == View.INVISIBLE)
                View.VISIBLE
            else
                View.INVISIBLE
    }

    private fun updateRv(dataSet: NewsFeedResponse) {
        binding.recyclerView.layoutManager = LinearLayoutManager(
            this, LinearLayoutManager.HORIZONTAL, false)
        val adapter = NewsAdapter(dataSet, ::openWebLink)
        binding.recyclerView.adapter = adapter
        adapter.notifyDataSetChanged()
    }

    private fun openWebLink(url: @ParameterName(name = "url") String) {

    }

    private fun initBinding() {
        binding = ActivityMainBinding.inflate(layoutInflater)
    }

    private fun initView() {
        binding.newsToolbar.title = getString(R.string.toolbar_title)
        setSupportActionBar(binding.newsToolbar)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        super.onCreateOptionsMenu(menu)
        menuInflater.inflate(R.menu.news_menu, menu)
        menu?.findItem(R.id.search_query)?.actionView?.let {
            val searchView = it as SearchView
            searchView.setOnQueryTextListener(QueryListener(searchView))
        }
        return true
    }

    inner class QueryListener(val searchView: SearchView) : SearchView.OnQueryTextListener {
        override fun onQueryTextSubmit(query: String?): Boolean {
            query?.let {
                viewModel.getNewsFeedQuery(query)
            }
            searchView.clearFocus()
            searchView.isIconified = false
            searchView.setIconifiedByDefault(false)
            return true
        }

        override fun onQueryTextChange(newText: String?): Boolean {
            return false
        }
    }
}