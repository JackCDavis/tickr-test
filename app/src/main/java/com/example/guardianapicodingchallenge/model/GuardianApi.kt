package com.example.guardianapicodingchallenge.model

import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

//https://content.guardianapis.com/
// search?
// q
// show-fields [trailText&headline&thumbnail]
//api-key
//order-by "newest"
//https://content.guardianapis.com/search?q=soccer&show-fields=trailText&headline&thumbnail&api-key=aefff39e-d07e-4ed8-8db2-ee33567e4f65
interface GuardianApi {
    @GET(ENDPOINT)
    fun getNewsFeed(
        @Query(QUERY_PARAM) query: String,
        @Query(SHOW_FIELDS_PARAM) showFields: String = SHOW_FIELDS_VAL,
        @Query(API_KEY_PARAM) apiKey: String = API_KEY_VAL,
        @Query(ORDER_BY_PARAM) orderBy: String = ORDER_BY_VAL
    ): Single<NewsFeedResponse>

    companion object {
        fun initRetrofit() =
            Retrofit.Builder()
                .client(createLogger())
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
                .build()
                .create(GuardianApi::class.java)

        fun createLogger(): OkHttpClient{
            val loggerInterceptor = HttpLoggingInterceptor()
            loggerInterceptor.level = HttpLoggingInterceptor.Level.BODY
            val okHttpClient = OkHttpClient.Builder()
                .addInterceptor(loggerInterceptor)
                .build()
            return okHttpClient
        }
    }
}