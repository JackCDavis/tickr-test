package com.example.guardianapicodingchallenge.model

const val BASE_URL = "https://content.guardianapis.com/"
const val ENDPOINT = "search"
const val QUERY_PARAM = "q"
const val SHOW_FIELDS_PARAM = "show-fields"
const val SHOW_FIELDS_VAL = "trailText,headline,thumbnail"
const val API_KEY_PARAM = "api-key"
const val ORDER_BY_PARAM = "order-by"
const val API_KEY_VAL = "aefff39e-d07e-4ed8-8db2-ee33567e4f65"
const val ORDER_BY_VAL = "newest"