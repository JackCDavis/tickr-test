package com.example.guardianapicodingchallenge.model

sealed class GuardianAppState {
    data class RESPONSE(val dataSet: NewsFeedResponse): GuardianAppState()
    object LOADING: GuardianAppState()
    data class ERROR(val errorMessage: String): GuardianAppState()
}