package com.example.guardianapicodingchallenge.model


import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.schedulers.Schedulers
import javax.inject.Inject

class GuardianRepository @Inject constructor( var guardianApi: GuardianApi ){

    fun searchQueryInput(query: String): Single<NewsFeedResponse> {
        return guardianApi.getNewsFeed(query)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
//        return GuardianApi.initRetrofit().getNewsFeed(query)
//            .subscribeOn(Schedulers.io())
//            .observeOn(AndroidSchedulers.mainThread())
    }
}