package com.example.guardianapicodingchallenge.model

import com.squareup.moshi.Json

data class NewsFeedResponse(
//    @Json(name = "response")
    val response: NewsFeedResult
)
data class NewsFeedResult(
//    @Json(name = "startIndex")
    val startIndex : Int,
//    @Json(name = "pageSize")
    val pageSize: Int,
    //@Json(name = "currentPage")
    val currentPage: Int,
//    @Json(name = "results")
    val results: List<ResultsInfo>
)

data class ResultsInfo(
//    @Json(name = "fields")
    val fields: FieldsResult,
//    @Json(name = "webUrl")
    val webUrl: String
)

data class FieldsResult(
//    @Json(name = "headline")
    val headline: String,
//    @Json(name = "trailText")
    val trailText: String,
//    @Json(name = "thumbnail")
    val thumbnail: String
)