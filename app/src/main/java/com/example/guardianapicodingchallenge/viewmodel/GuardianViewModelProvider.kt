package com.example.guardianapicodingchallenge.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.guardianapicodingchallenge.model.GuardianRepository
import javax.inject.Inject


class GuardianViewModelProvider @Inject constructor(val repository: GuardianRepository): ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return GuardianViewModel(repository) as T
    }
}
