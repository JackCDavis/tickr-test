package com.example.guardianapicodingchallenge.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.guardianapicodingchallenge.model.GuardianAppState
import com.example.guardianapicodingchallenge.model.GuardianRepository
import javax.inject.Inject

private const val TAG = "GuardianViewModel"

class GuardianViewModel(val repository: GuardianRepository): ViewModel() {

    private val mutableLiveData = MutableLiveData<GuardianAppState>()

    fun getNewsFeedData(): LiveData<GuardianAppState>{
        return mutableLiveData
    }

    fun getNewsFeedQuery(query: String){
        mutableLiveData.value = GuardianAppState.LOADING

        repository.searchQueryInput(query)
            .doAfterSuccess { mutableLiveData.value = GuardianAppState.LOADING }
            .subscribe({dataResponse->
                mutableLiveData.value = GuardianAppState.RESPONSE(dataResponse)
            },{errorMessage->
                mutableLiveData.value = GuardianAppState.ERROR(errorMessage.message?: "Error")
            })
    }
}